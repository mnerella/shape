import java.util.Scanner;

/**
 *  
 * @author mnerella
 *
 */

public class Main {
public static void main(String[] args) {
	Scanner scn= new Scanner(System.in);
//Select the choice ,What users want
System.out.print("Enter  your choice:");
int ch=scn.nextInt();
switch(ch) {
case 1: //This is Circle case to perform Circle Operations
	    System.out.println("Circle operations");
	    Circle c=new Circle();
        c.perimeter();
        break;

case 2: //This is Rectangle case to perform Rectangle Operations
	    System.out.println("Rectangle operations");
	    Rectangle rec=new Rectangle();
        rec.perimeter();
        break;

case 3: //This is Square case to perform Square Operations
	    System.out.println("Square operations");
	    Square s=new Square();
        s.perimeter();
        break;
        
}


}
}