import java.util.Scanner;

/**
 * This is a interface i.e shape to perform perimeter,Area Diameter methods
 * @author mnerella
 *
 */
public interface Shape {
	
	
	public void perimeter();//Perimeter 

	public void Area();//Area

	public void Diameter();//Diameter
}

//Create a class Circle which implements interface i.e Shape
class Circle implements Shape {
	int radius = 4;
	double pi = 3.14, perimeter, area;
	Scanner scn = new Scanner(System.in);
 
	//Override the perimeter method from Shape interface
	@Override 
	public void perimeter() {
		// Calculate the perimeter of circle
		perimeter = 2 * pi * radius;
		System.out.println("Perimeter of circle:" + perimeter);
		Area();
	}
 
	//Override the Area method from Shape interface
	@Override
	public void Area() {
		// Calculate the area of circle
		area = pi * radius * radius;
		System.out.println("Area of circle:" + area);
		Diameter();

	}
    
	//Override the Diameter method from Shape interface
	@Override
	public void Diameter() {
		// Calculate the diameter of circle
		int diameter = 2 * radius;
		System.out.println("Diameter of circle:" + diameter);

	}

}
/**
 * This is an abstract class i.e AbstractRectangle which implements i.e shape
 * @author mnerella
 *
 */
abstract class AbstractRectangle implements Shape {

//     public abstract void perimeter();
//     public abstract void Area();
//     public abstract void Diameter();
}
/**
 * Create a class Rectangle which performs some operations from  AbstractRectangle(abstract class) 
 * @author agannarapu
 *
 */
class Rectangle extends AbstractRectangle {
	int length = 10;
	int breadth = 20;
 
	//Override the Perimeter method from Rectangle class
	@Override
	public void perimeter() {
        //Formula to find Perimeter of Rectangle
		int perimeter = 2 * length * breadth;
		System.out.println("Perimeter of Rectangle:" + perimeter);
		Area();

	}

	//Override the Area method from Rectangle class
	@Override
	public void Area() {
		//Formula to find Area of Rectangle
		int area = length * breadth;
		System.out.println("Area of Rectangle:" + area);
		Diameter();

	}
    
	//Override the Diameter method from Rectangle class
	@Override
	public void Diameter() {
		//Formula to find Length of diagonal of Rectangle
		double diagonal = Math.sqrt((length * length) + (breadth * breadth));
		System.out.println("Length of diagonal:" + diagonal);

	}

}

/**
 * Create a class Square which performs some operations from  AbstractRectangle(abstract class) 
 * @author mnerella
 *
 */
class Square extends AbstractRectangle {
	int side = 5;
    
	//Override the Perimeter method from Square class
	@Override
	public void perimeter() {
		//Formula to find Perimeter of square
		int perimeter = 4 * side;
		System.out.println("Perimeter  of square is: " + perimeter);
		Area();
	}
 
	//Override the Area method from Square class
	@Override
	public void Area() {
		//Formula to find Area of square
		int area = side * side;
		System.out.println("Area of square:" + area);
		Diameter();

	}
    
	//Override the Diameter method from Square class
	@Override
	public void Diameter() {
		//Formula to find length of diagonal of square
		double diagonal = Math.sqrt(2) * side;
		System.out.println("Length of Diagonal:" + diagonal);

	}

}